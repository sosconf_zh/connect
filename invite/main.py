#!/usr/bin/python3

from PIL import Image, ImageDraw, ImageFont
import os

namelist = ["成电LUG", "清华大学TUNA 协会", "中科大LUG"]
for name in namelist :
    pic = Image.open("invite.jpg")
    draw = ImageDraw.Draw(pic)
    thefont = ImageFont.truetype("font.ttf", size=32)
    draw.text((82,320), name+":", font=thefont, fill='black')
    pic.save(os.path.join("dist", name+".jpg"), "JPEG")
